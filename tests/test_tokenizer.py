from seq2seq_summarization.dataset.tokenizer import Tokenizer
from seq2seq_summarization.dataset.seq_dataset import SeqDataset
import pickle
import pytest
from pathlib import Path


@pytest.fixture
def read_inputs():
    with open("tests/test_data/train_input.pickle", "rb") as handle:
        train = pickle.load(handle)
    with open("tests/test_data/val_input.pickle", "rb") as handle:
        val = pickle.load(handle)
    return train, val


@pytest.fixture
def read_outputs():
    with open("tests/test_data/corpus_train.txt", "r") as f:
        corpus_train = f.read()
    with open("tests/test_data/corpus_train_val.txt", "r") as f:
        corpus_train_val = f.read()
    return corpus_train, corpus_train_val


def test_building_corpus_train(read_inputs, read_outputs):
    train, _ = read_inputs
    corpus_train_expected, _ = read_outputs
    tok = Tokenizer()
    corpus_train_actual = "".join(tok.build_corpus(train))
    assert corpus_train_expected == corpus_train_actual


def test_building_corpus_train_val(read_inputs, read_outputs):
    train, val = read_inputs
    _, corpus_train_val_expected = read_outputs
    tok = Tokenizer()
    corpus_train_val_actual = "".join(tok.build_corpus(train, val))
    assert corpus_train_val_expected == corpus_train_val_actual


def test_trained_vocab_length(read_inputs):
    train, val = read_inputs
    tok = Tokenizer()
    vocab = "".join(tok.build_corpus(train, val)).split(" ")
    vocab_unique = set(vocab)
    assert (
        len(vocab_unique)
        <= tok.train_tokenizer(
            "tests/test_data/corpus_train_val.txt", 30000
        ).get_vocab_size()
    )


def test_pad_idx_equals_0():
    tokenizer = Tokenizer()
    assert tokenizer.get_pad_idx("tests/test_data/vocab.txt") == 0
