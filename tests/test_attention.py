import torch
import pytest
from seq2seq_summarization.model.seq2seq_testcase import Seq2SeqTestCase
from seq2seq_summarization.model.attention import Attention


class TestAttention(Seq2SeqTestCase):
    encoder_hidden_size = 20
    decoder_hidden_size = 30
    input_seq_len = 50

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_attention_return_shape(self, batch_size):

        attn = Attention(self.encoder_hidden_size, self.decoder_hidden_size)

        decoder_hidden = self._get_floattensor_with_shape(
            batch_size, self.decoder_hidden_size
        )
        encoder_outputs = self._get_floattensor_with_shape(
            batch_size, self.input_seq_len, self.encoder_hidden_size
        )

        attn_results = attn.forward(decoder_hidden, encoder_outputs)

        print(attn_results)
        assert torch.allclose(attn_results.sum(dim=1), torch.ones(batch_size))
        self.assert_tensor_has_shape(attn_results, (batch_size, self.input_seq_len))
