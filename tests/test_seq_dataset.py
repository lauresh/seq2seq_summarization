from seq2seq_summarization.dataset.seq_dataset import SeqDataset
from torch.utils.data import DataLoader
from seq2seq_summarization.dataset.tokenizer import Tokenizer
import pytest
from pathlib import Path


@pytest.fixture
def example_texts():
    return [
        (
            "COVID-19 affects different people in different ways. Most infected people will develop mild to moderate illness and recover without hospitalization.",
            "coronavirus infection",
        ),
        (
            "Automatic summarization is the process of shortening a set of data computationally, to create a subset that represents the most important or relevant information within the original content. In addition to text, images and videos can also be summarized.",
            "automatic summarization",
        ),
        (
            "The chocolate production process consists of fermentation, drying, roasting, grinding of cocoa beans, mixing of all ingredients (cocoa mass, sugar, cocoa butter, emulsifiers, aroma, and milk components if needed), conching, and tempering.",
            "chocolate production",
        ),
        (
            "Software development is the process programmers use to build computer programs. The process, also known as the Software Development Life Cycle (SDLC), includes several phases that provide a method for building products that meet technical specifications and user requirements.",
            "software develpment",
        ),
    ]


def test_getitem(example_texts):
    tokenizer = Tokenizer().load_trained_tokenizer("tests/test_data/vocab.txt")
    ex_texts = example_texts
    dataset = SeqDataset(ex_texts, tokenizer)
    assert dataset[0] == (
        "COVID-19 affects different people in different ways. Most infected people will develop mild to moderate illness and recover without hospitalization.",
        "coronavirus infection",
    )


def test_encoding_decoding(example_texts):
    tokenizer = Tokenizer().load_trained_tokenizer("tests/test_data/vocab.txt")
    texts = example_texts
    dataset = SeqDataset(texts, tokenizer)
    loader = DataLoader(dataset, batch_size=4, collate_fn=dataset.collate_sequences)
    encoded = next(iter(loader))
    expected = "COVID - 19 affects different people in different ways. Most infected people will develop mild to moderate illness and recover without hospitalization."
    actual = dataset.decode(encoded[0])[0]
    assert expected == actual
