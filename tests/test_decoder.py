import pytest
import torch
from seq2seq_summarization.model.attention import Attention
from seq2seq_summarization.model.decoder import Decoder
from seq2seq_summarization.model.encoder import Encoder
from seq2seq_summarization.model.seq2seq_testcase import Seq2SeqTestCase


class TestDecoder(Seq2SeqTestCase):
    embedding_size = 50
    target_vocab_size = 5
    context_size = 128
    max_target_seq_len = 30
    input_seq_len = 58

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_decoder_eval_without_attention(self, batch_size):

        decoder = Decoder(
            embedding_size=self.embedding_size,
            target_vocab_size=self.target_vocab_size,
            context_size=self.context_size,
            start_token_idx=1,
        )
        decoder.eval()

        context_vector = torch.rand((batch_size, self.context_size))

        results = decoder.forward(
            context_vector, max_target_seq_len=self.max_target_seq_len
        )

        assert "outputs" in results.keys()
        assert "predictions" in results.keys()
        outputs = results["outputs"]
        predictions = results["predictions"]

        self.assert_tensor_has_shape(
            outputs, (batch_size, self.max_target_seq_len, self.target_vocab_size)
        )
        self.assert_tensor_has_shape(predictions, (batch_size, self.max_target_seq_len))

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_decoder_train_without_attention(self, batch_size):

        decoder = Decoder(
            embedding_size=self.embedding_size,
            target_vocab_size=self.target_vocab_size,
            context_size=self.context_size,
            start_token_idx=1,
        )
        decoder.train()

        context_vector = self._get_floattensor_with_shape(batch_size, self.context_size)
        target_sequence = self._get_longtensor_with_shape(
            self.target_vocab_size, (batch_size, self.max_target_seq_len)
        )

        results = decoder.forward(context_vector, target_sequence=target_sequence)

        assert "outputs" in results.keys()
        assert "predictions" in results.keys()
        outputs = results["outputs"]
        predictions = results["predictions"]

        self.assert_tensor_has_shape(
            outputs, (batch_size, self.max_target_seq_len, self.target_vocab_size)
        )
        self.assert_tensor_has_shape(predictions, (batch_size, self.max_target_seq_len))

        criterion = torch.nn.CrossEntropyLoss()
        loss = criterion(results["outputs"].transpose(1, 2), target_sequence)
        loss.backward()

        for name, param in decoder.named_parameters():
            if name == "fc.bias":
                print(param.grad)
            self.assert_parameter_has_grad(param)

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_decoder_eval_with_attention(self, batch_size):

        attn = Attention(
            encoder_hidden_size=self.context_size * 2,
            decoder_hidden_size=self.context_size,
        )

        decoder = Decoder(
            embedding_size=self.embedding_size,
            target_vocab_size=self.target_vocab_size,
            context_size=self.context_size,
            start_token_idx=1,
            attention=attn,
        )
        decoder.eval()

        context_vector = self._get_floattensor_with_shape(batch_size, self.context_size)
        encoder_outputs = self._get_floattensor_with_shape(
            batch_size, self.input_seq_len, self.context_size * 2
        )

        results = decoder.forward(
            context_vector,
            max_target_seq_len=self.max_target_seq_len,
            encoder_outputs=encoder_outputs,
        )

        assert "outputs" in results.keys()
        assert "predictions" in results.keys()
        assert "attention" in results.keys()
        outputs = results["outputs"]
        predictions = results["predictions"]
        attention = results["attention"]

        self.assert_tensor_has_shape(
            outputs, (batch_size, self.max_target_seq_len, self.target_vocab_size)
        )
        self.assert_tensor_has_shape(predictions, (batch_size, self.max_target_seq_len))

        self.assert_tensor_has_shape(
            attention, (batch_size, self.max_target_seq_len, self.input_seq_len)
        )

        assert torch.allclose(
            results["attention"].sum(dim=2).squeeze(),
            torch.ones((batch_size, self.max_target_seq_len)),
        )

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_decoder_train_with_attention(self, batch_size):

        attn = Attention(
            encoder_hidden_size=self.context_size * 2,
            decoder_hidden_size=self.context_size,
        )

        decoder = Decoder(
            embedding_size=self.embedding_size,
            target_vocab_size=self.target_vocab_size,
            context_size=self.context_size,
            start_token_idx=1,
            attention=attn,
        )
        decoder.train()

        context_vector = self._get_floattensor_with_shape(batch_size, self.context_size)
        target_sequence = self._get_longtensor_with_shape(
            self.target_vocab_size, (batch_size, self.max_target_seq_len)
        )
        encoder_outputs = self._get_floattensor_with_shape(
            batch_size, self.input_seq_len, self.context_size * 2
        )

        results = decoder.forward(
            context_vector,
            target_sequence=target_sequence,
            encoder_outputs=encoder_outputs,
        )

        criterion = torch.nn.CrossEntropyLoss()
        loss = criterion(results["outputs"].transpose(1, 2), target_sequence)
        loss.backward()

        assert loss != 0
        for name, param in decoder.named_parameters():
            if name == "fc.bias":
                print(param.grad)
            self.assert_parameter_has_grad(param)

        for param in attn.parameters():
            self.assert_parameter_has_grad(param)
