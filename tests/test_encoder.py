import torch
from seq2seq_summarization.model.encoder import Encoder
import pytest
from seq2seq_summarization.model.seq2seq_testcase import Seq2SeqTestCase


class TestEncoder(Seq2SeqTestCase):
    embedding_size = 30
    vocab_size = 5
    context_size = 60
    seq_len = 25
    padding_idx = 0
    is_bidirectional = True

    @pytest.mark.parametrize("batch_size", [(1), (64)])
    def test_encoder_return_shape(self, batch_size):

        x = self._get_longtensor_with_shape(self.vocab_size, (batch_size, self.seq_len))

        encoder_layer = Encoder(
            context_size=self.context_size,
            embedding_size=self.embedding_size,
            input_vocab_size=self.vocab_size,
            padding_idx=self.padding_idx,
            is_bidirectional=self.is_bidirectional,
        )

        result = encoder_layer(x)
        outputs = result["outputs"]
        context = result["context"]

        self.assert_tensor_has_shape(
            outputs, (batch_size, self.seq_len, self.context_size * 2)
        )
        self.assert_tensor_has_shape(context, (batch_size, self.context_size))
