import gzip
import json
import os
from itertools import chain
from pathlib import Path
from typing import List


class DataBuilder:
    def __init__(self, input_path: Path, split_type: str, cpc_code: str):
        """
        Class to prepare Big Patent dataset for summarization.
        :param input_path: input to folder with data
        :param split_type: 3 options: train, val, test
        :param cpc_code: group of patents (options: a, b, c, d, e, f, g, h, y)
        """
        self.input_path = input_path
        if split_type not in ["train", "val", "test"]:
            raise ValueError(
                "Value for split_type should be one of ['train', 'val', 'test']"
            )
        self.split_type = split_type
        if cpc_code not in ["a", "b", "c", "d", "e", "f", "g", "h", "y"]:
            raise ValueError(
                "Value for cpc_code should be one of ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'y']"
            )
        self.cpc_code = cpc_code

    def _read_data(self) -> List[List[dict]]:
        """
        Method to read data of specific split type and cpc code.
        :return: List of lists of dictionaries with data
        """
        file_names = os.listdir(
            os.path.join(self.input_path, self.split_type, self.cpc_code)
        )
        all_data = []
        for file in file_names:
            print(
                "Reading file "
                + file
                + " from "
                + self.split_type
                + " split for cpc code "
                + self.cpc_code
            )
            with gzip.GzipFile(
                os.path.join(self.input_path, self.split_type, self.cpc_code, file), "r"
            ) as fin:
                data_file = []
                for row in fin:
                    data_file.append(json.loads(row))
                all_data.append(data_file)
        return all_data

    def prepare_data(self):
        """
        Method to transform data into a list of tuples containing only relevant fields: description and abstract.
        :return: List of tuples
        """
        data = self._read_data()
        data_chained = list(chain.from_iterable(data))
        return list(
            zip(
                [x["description"] for x in data_chained],
                [x["abstract"] for x in data_chained],
            )
        )
