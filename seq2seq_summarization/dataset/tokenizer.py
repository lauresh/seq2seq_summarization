from tokenizers.implementations import BertWordPieceTokenizer
from tokenizers.processors import BertProcessing
from typing import List, Tuple
from pathlib import Path
import collections


class Tokenizer:
    def __init__(self):
        """
        Class to prepare corpus and train tokenizer on it.
        """
        self.tokenizer = self._instantiate_tokenizer()

    def build_corpus(
        self, train_data: List[Tuple[str, str]], val_data: List[Tuple[str, str]] = None
    ):
        if val_data:
            corpus = [elem[0] + elem[1] for elem in train_data + val_data]
        else:
            corpus = [elem[0] + elem[1] for elem in train_data]
        return corpus

    def save_corpus(self, output_path: str, corpus: List[str]):
        with open(output_path, "w") as f:
            f.writelines(corpus)

    def _instantiate_tokenizer(self, vocab_path: str = None):
        if vocab_path:
            tokenizer = BertWordPieceTokenizer(
                vocab_path, lowercase=False, handle_chinese_chars=False
            )
        else:
            tokenizer = BertWordPieceTokenizer(
                lowercase=False, handle_chinese_chars=False
            )
        return tokenizer

    def train_tokenizer(self, corpus_path: str, vocab_size: int):
        self.tokenizer.train(corpus_path, vocab_size=vocab_size)
        return self.tokenizer

    def save_tokenizer(self, output_path: str):
        self.tokenizer.save_model(output_path)

    def load_trained_tokenizer(self, vocab_path: str):
        tokenizer = self._instantiate_tokenizer(vocab_path=vocab_path)
        tokenizer.post_processor = BertProcessing(
            ("[CLS]", tokenizer.token_to_id("[CLS]")),
            ("[SEP]", tokenizer.token_to_id("[SEP]")),
        )
        tokenizer.enable_padding(pad_id=tokenizer.token_to_id("[PAD]"))
        # tokenizer.enable_truncation(64)
        return tokenizer

    def get_vocab_size(self, vocab_path: str):
        tokenizer = self.load_trained_tokenizer(vocab_path=vocab_path)
        return tokenizer.get_vocab_size()

    def get_pad_idx(self, vocab_path: str):
        tokenizer = self.load_trained_tokenizer(vocab_path=vocab_path)
        return tokenizer.token_to_id("[PAD]")

    def get_start_idx(self, vocab_path: str):
        tokenizer = self.load_trained_tokenizer(vocab_path=vocab_path)
        return tokenizer.token_to_id("[CLS]")
