from seq2seq_summarization.dataset.data_builder import DataBuilder
from seq2seq_summarization.dataset.tokenizer import Tokenizer
from seq2seq_summarization.dataset.seq_dataset import SeqDataset
from pathlib import Path
import pandas as pd

if __name__ == "__main__":
    data_train = DataBuilder(
        Path("seq2seq_summarization/dataset/bigPatentData/"), "train", "d"
    ).prepare_data()
    # with open ("data/data_train.csv", "w") as f:
    #     f.write(data_train)
    data_val = DataBuilder(
        Path("seq2seq_summarization/dataset/bigPatentData/"), "val", "d"
    ).prepare_data()
    # with open ("data/data_val.csv", "w") as f:
    #     f.write(data_train)
    data_test = DataBuilder(
        Path("seq2seq_summarization/dataset/bigPatentData/"), "test", "d"
    ).prepare_data()

    # tok = Tokenizer()
    # corpus = tok.build_corpus(data_train, data_val)
    # tok.save_corpus(Path("data/corpus.txt"), corpus)
    # tokenizer = tok.train_tokenizer(Path("data/corpus.txt"), 10000)
    # tok.save_tokenizer(".")
    # end of training tokenizer

    # modelling part
    # dataset_train = SeqDataset(data_train, tokenizer=tokenizer)
    # padding_idx = dataset_train.get_pad_idx()

# test from root of the project with: python -m seq2seq_summarization.dataset.run_data
