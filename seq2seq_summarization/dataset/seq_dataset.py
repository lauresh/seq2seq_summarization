import torch
from tokenizers.implementations import BertWordPieceTokenizer
from torch.utils.data import Dataset


class SeqDataset(Dataset):
    def __init__(self, texts: list, tokenizer: BertWordPieceTokenizer):
        """
        Build tensor dataset. Encode and pad in batches (padding to the longest sequence in batch)
        :param tokenizer: trained tokenizer
        :param texts: list of tuples with description and abstract (x and y)
        """

        self.tokenizer = tokenizer
        self.texts = texts

    # def get_vocab_size(self): # moved to tokenizer class
    #     return self.tokenizer.get_vocab_size()
    #
    # def get_pad_idx(self):
    #     return self.tokenizer.token_to_id("[PAD]")

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        x = self.texts[idx][0]
        y = self.texts[idx][1]

        return x, y

    def collate_sequences(self, batch):
        x, y = zip(*batch)

        x = list(x)
        y = list(y)

        x_encoded = self.tokenizer.encode_batch(x, add_special_tokens=True)
        y_encoded = self.tokenizer.encode_batch(y, add_special_tokens=True)

        x_encoded_tensor = []
        y_encoded_tensor = []

        for elem in x_encoded:
            x_encoded_tensor.append(elem.ids)

        for elem in y_encoded:
            y_encoded_tensor.append(elem.ids)

        return torch.LongTensor(x_encoded_tensor), torch.LongTensor(y_encoded_tensor)

    def decode(self, encoded: torch.LongTensor):
        decoded = self.tokenizer.decode_batch(
            encoded.tolist(), skip_special_tokens=True
        )

        return decoded
