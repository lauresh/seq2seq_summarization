import pytorch_lightning as pl
from seq2seq_summarization.model.seq2seq_model import Seq2SeqModel
from seq2seq_summarization.dataset.seq_dataset import SeqDataset
from torch.utils.data.dataloader import DataLoader
from seq2seq_summarization.dataset.data_builder import DataBuilder
from seq2seq_summarization.dataset.tokenizer import Tokenizer
from pathlib import Path
import torch

model = Seq2SeqModel.load_from_checkpoint(
    "lightning_logs/version_14/checkpoints/epoch=50.ckpt", map_location="cpu"
)

model.freeze()
model.eval()

test_dataset = SeqDataset(
    texts=DataBuilder(Path("../dataset/bigPatentData/"), "test", "d").prepare_data(),
    tokenizer=Tokenizer().load_trained_tokenizer("../dataset/data/vocab.txt"),
)

test_loader = DataLoader(
    test_dataset,
    batch_size=16,
    shuffle=False,
    collate_fn=test_dataset.collate_sequences,
)

x, y = next(iter(test_loader))
preds = model.prediction((x, y), 0)
decoded = dset.text_tokenizer.decode(torch.cat([x[0] for x in preds]))
print(zero_decoded)
print(dset.df["text"][0])
