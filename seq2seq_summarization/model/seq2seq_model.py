from argparse import ArgumentParser
import pytorch_lightning as pl
import torch
import torch.nn as nn
from torch.utils.data.dataloader import DataLoader
from seq2seq_summarization.model.attention import Attention
from seq2seq_summarization.model.encoder import Encoder
from seq2seq_summarization.model.decoder import Decoder
from seq2seq_summarization.dataset.seq_dataset import SeqDataset
from seq2seq_summarization.dataset.data_builder import DataBuilder
from seq2seq_summarization.dataset.tokenizer import Tokenizer
from pathlib import Path
from datasets import load_metric

metric = load_metric("rouge")


class Seq2SeqModel(pl.LightningModule):
    def __init__(self, hparams):
        super(Seq2SeqModel, self).__init__()
        self.hparams = hparams
        self.tokenizer = Tokenizer()
        self.val_dataset = SeqDataset(
            texts=DataBuilder(
                Path("../dataset/bigPatentData/"), "val", "d"
            ).prepare_data(),
            tokenizer=Tokenizer().load_trained_tokenizer("../dataset/data/vocab.txt"),
        )
        self.train_dataset = SeqDataset(
            texts=DataBuilder(
                Path("../dataset/bigPatentData/"), "train", "d"
            ).prepare_data(),
            tokenizer=Tokenizer().load_trained_tokenizer("../dataset/data/vocab.txt"),
        )

        self.encoder = Encoder(
            context_size=hparams.context_size,
            embedding_size=hparams.encoder_embedding_size,
            is_bidirectional=hparams.is_bidirectional,
            input_vocab_size=self.tokenizer.get_vocab_size("../dataset/data/vocab.txt"),
            padding_idx=self.tokenizer.get_pad_idx("../dataset/data/vocab.txt"),
            dropout=hparams.encoder_dropout,
        )

        self.attention = Attention(self.encoder.get_hidden_size(), hparams.context_size)

        self.decoder = Decoder(
            embedding_size=hparams.decoder_embedding_size,
            target_vocab_size=self.tokenizer.get_vocab_size(
                "../dataset/data/vocab.txt"
            ),
            context_size=hparams.context_size,
            start_token_idx=self.tokenizer.get_start_idx("../dataset/data/vocab.txt"),
            attention=self.attention,
        )

        # loss
        self.criterion = nn.CrossEntropyLoss(ignore_index=0)

    def forward(self, x, y):

        encoder_results = self.encoder(x)
        decoder_results = self.decoder.forward(
            context_vector=encoder_results["context"],
            encoder_outputs=encoder_results["outputs"],
            target_sequence=y,
        )

        loss = self.criterion(decoder_results["outputs"].transpose(1, 2), y)

        return {
            "loss": loss,
            "output": decoder_results["outputs"],
            "predictions": decoder_results["predictions"],
            "attention": decoder_results["attention"],
        }

    def training_step(self, batch, batch_idx):
        x, y = batch
        results = self.forward(x, y)

        return {"loss": results["loss"], "log": {"train-loss": results["loss"]}}
        # CZY POWINNAM DODAC TEZ LICZENIE ROUGE SCORE DO TRAINING STEP?

    def validation_step(self, batch, batch_idx):
        x, y = batch
        results = self.forward(x, y)
        metric.add_batch(
            predictions=self.val_dataset.decode(results["predictions"]),
            references=self.val_dataset.decode(x),
        )
        score = metric.compute(
            rouge_types=["rouge1"], use_agregator=False
        )  # CZY TO POWINNO BYC W TYM MIEJSCU??
        score_avg_batch = torch.mean(
            torch.FloatTensor([elem.fmeasure for elem in score["rouge1"]])
        )

        return {"loss": results["loss"], "log": {"rouge": score_avg_batch}}

    def validation_epoch_end(self, outputs):
        loss = torch.stack([x["loss"] for x in outputs]).mean()
        rouge_val = torch.stack([x["log"]["rouge"] for x in outputs]).mean()
        print(rouge_val)

        return {"log": {"val-loss": loss, "val-rouge": rouge_val}}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters())

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument("--context_size", type=int, default=128)
        parser.add_argument("--encoder_embedding_size", type=int, default=32)
        parser.add_argument("--encoder_dropout", type=float, default=0.1)
        parser.add_argument("--decoder_embedding_size", type=int, default=32)
        parser.add_argument("--batch_size", type=int, default=64)
        parser.add_argument("--is_bidirectional", type=bool, default=True)

        return parser

    def train_dataloader(self):
        dset = self.train_dataset
        loader = DataLoader(
            dset,
            batch_size=self.hparams.batch_size,
            shuffle=True,
            collate_fn=dset.collate_sequences,
        )
        return loader

    def val_dataloader(self):
        dset = self.val_dataset
        loader = DataLoader(
            dset,
            batch_size=self.hparams.batch_size,
            shuffle=False,
            collate_fn=dset.collate_sequences,
        )
        return loader


if __name__ == "__main__":
    parser = ArgumentParser()
    parser = Seq2SeqModel.add_model_specific_args(parser)
    args = parser.parse_args()
    model = Seq2SeqModel(args)

    trainer = pl.Trainer(
        # gpus=[],
        gradient_clip_val=1.0,
        max_epochs=50,
        fast_dev_run=False,
        # logger=NeptuneLogger(project_name="/seq2seq_summarization"),
    )
    trainer.fit(model)

# TODO: docker image
# TODO: train on GPU
# TODO: save checkoint and write code for prediction
