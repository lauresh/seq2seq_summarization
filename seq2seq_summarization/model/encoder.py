import torch
import torch.nn as nn
import pytorch_lightning as pl
from torch.utils.data.dataloader import DataLoader

# from pytorch_lightning.logging import NeptuneLogger
from seq2seq_summarization.dataset.seq_dataset import SeqDataset
from typing import Dict
import torch
import torch.nn as nn


class Encoder(nn.Module):
    def __init__(
        self,
        context_size: int,
        embedding_size: int,
        is_bidirectional: bool,
        input_vocab_size: int,
        padding_idx: int,
        dropout: float = 0.0,
    ):
        """A bidirectional GRU encoder for seq2seq model.
        Takes an input sequence and turns it into a fixed-size context vector
        :param context_size: Size of the context vector
        :param embedding_size: Size of the encoder embedding
        :param is_bidirectional: whether to use bidirectional GRU layers
        :param input_vocab_size: Vocabulary size for source sequence
        :param padding_idx: Index of the padding token in source sequence
        :param dropout: Dropout value, defaults to 0.0
        """
        super().__init__()
        self.is_bidirectional = is_bidirectional

        if is_bidirectional:
            self.hidden_size = 2 * context_size
        else:
            self.hidden_size = context_size
        self.embedding = nn.Embedding(
            num_embeddings=input_vocab_size,
            embedding_dim=embedding_size,
            padding_idx=padding_idx,
        )

        self.encoder_rnn = nn.GRU(
            input_size=embedding_size,
            hidden_size=context_size,
            batch_first=True,
            bidirectional=is_bidirectional,
        )

        self.fc = nn.Linear(self.hidden_size, context_size)
        self.tanh = nn.Tanh()
        self.dropout = nn.Dropout(dropout)

    def get_hidden_size(self):
        return self.hidden_size

    def forward(self, source_sequence: torch.Tensor) -> Dict[str, torch.Tensor]:
        """Forwards the encoder
        :param source_sequence: Tensor with encoded input sequence.
            Shape: batch size x input_seq_len x src_vocab_size
        :return: Dictionary with encoder outputs (for attention) and context vector
            Shapes:
                outputs: batch size x input_seq_len x context size * num_directions
                context: batch size x context_size
        """

        embedded = self.embedding(source_sequence)
        outputs, encoded = self.encoder_rnn(embedded)

        encoded = torch.cat((encoded[0, :, :], encoded[1, :, :]), dim=1)

        encoded = self.fc(encoded)
        encoded = self.tanh(encoded)
        encoded = self.dropout(encoded)

        return {"outputs": outputs, "context": encoded}
