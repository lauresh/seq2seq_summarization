from typing import Tuple
import torch
import torch.nn as nn
from assertpy import assert_that
from torch import Tensor


class Seq2SeqTestCase:
    def _get_longtensor_with_shape(self, high_range, *shape):
        return torch.randint(0, high_range, *shape)

    def _get_floattensor_with_shape(self, *shape):
        return torch.randn(*shape)

    def assert_tensor_has_shape(
        self, tensor: torch.Tensor, shape: Tuple[int]
    ):  # jak zrobic oznaczenie typu tak zeby tupla mogla przyjmowac dowolna liczbe intow?
        assert_that(tensor).is_type_of(torch.Tensor)
        assert_that(tensor.shape).is_equal_to(shape)

    def assert_parameter_has_grad(self, parameter: nn.Parameter):
        assert_that(parameter).is_type_of(nn.Parameter)
        assert_that(parameter.grad).is_not_none()
        assert_that(
            parameter.grad.abs().sum().item()
        ).is_not_zero()  # using absolute value to prevent sum of non zero grads from summing up to 0

    def assert_tensor_does_not_require_grad(self, tensor: Tensor):
        assert_that(tensor).is_type_of(Tensor)
        assert_that(tensor).is_not_none()
        assert_that(tensor.requires_grad).is_false()
